import { createRouter, createWebHistory } from 'vue-router'

import { pages } from '@/services/Pages'

const routes : Array<any> = []

for (const key in pages) {
  routes.push({
    path: pages[key].href,
    name: pages[key].title,
    component: pages[key].view
  })
}

const history = createWebHistory()

const router = createRouter({
  history,
  routes
})

export default router
