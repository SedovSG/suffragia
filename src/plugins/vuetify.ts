/* eslint-disable no-unused-vars */
// Styles
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

// Vuetify
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

const kprfTheme = {
  dark: false,
  variables: {},
  colors: {
    background: '#ffffff',
    primary: '#c62828',
    'primary-darken-1': '#e53935',
    secondary: '#304ffe',
    'secondary-darken-1': '3949ab',
    accent: '#8e24aa',
    surface: '#eeeeee',
    error: '#ff1744',
    info: '#2979ff',
    success: '#00e676',
    warning: '#cddc39'
  }
}

export default createVuetify({
  theme: {
    defaultTheme: 'kprfTheme',
    themes: {
      kprfTheme
    }
  },
  components,
  directives
})
