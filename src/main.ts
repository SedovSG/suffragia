import { createApp } from 'vue'
import { vue3Debounce } from 'vue-debounce'
import vueTheMask from 'vue-the-mask'
import App from './App.vue'
import pinia from './plugins/pinia'
import router from './plugins/routes'
import vuetify from './plugins/vuetify'
import { loadFonts } from './plugins/webfontloader'
import './styles/main.scss'

loadFonts()

createApp(App)
  .use(vuetify)
  .use(pinia)
  .use(router)
  .use(vueTheMask)
  .directive('debounce', vue3Debounce({ trim: true, lock: true }))
  .mount('#app')
