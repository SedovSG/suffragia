export type QrCodeRegexpGroups = {
	all?: RegExpMatchArray | null;
	general: RegExpMatchArray | null;
	voting: RegExpMatchArray | null,
	rating: RegExpMatchArray[] | null,
}
