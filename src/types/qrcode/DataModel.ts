export type Election = {
  date: string,
  name: string,
  pronetvd: string,
  subjCode: string,
  vidvibref: string,
  vrn: string,
}

export type Candidate = {
  datroj: string,
  fio: string,
  index: number,
  namio: string,
  numokr: number,
  registr: string,
  tekstat2: string,
  vidvig: string,
  vrn: bigint,
  vrnio: bigint,
  votes: number
}

export type QrCodeDataModel = {
  // Дата проведения референдума
  date?: any;
  // Идентификатор УИК
  uik_id?: string;
  // Номер УИК
  uik_number: string,
  // Число избирателей на момент окончания голосования
  num_voters?: string;
  // Число бюллетеней, полученных УИК
  num_ballots?: string;
  // Число утраченных бюллетеней
  num_lost_ballots?: string;
  // Число бюллетеней для голосования досрочно
  num_early_ballots?: string;
  // Число действительных бюллетеней
  num_valid_ballots?: string;
  // Число бюллетеней для голосования в помещении
  num_ballots_indoor?: string;
  // Число уничтоженных бюллетеней
  num_destoy_ballots?: string;
  // Число бюллетеней в стационарных ящиках
  num_ballots_in_boxes?: string;
  // Число недействительных бюллетеней
  num_invalid_ballots?: string;
  // Число бюллетеней для голосования вне помещения
  num_ballots_outdoor?: string;
  // Число неучтённых бюллетеней
  num_unaccounted_ballots?: string;
  // Число бюллетеней в переносных ящиках
  num_ballots_in_portable_boxes?: string;
  // Рейтинги кандидатов
  candidate_rating?: RegExpMatchArray[];
  // Список кандидатов и голосов
  candidate_list?: { item: Candidate }[];
}
