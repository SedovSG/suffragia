import home from '@/views/PageHome.vue'
import protocol from '@/views/PageProtocol.vue'
import protocols from '@/views/PageProtocols.vue'
import total from '@/views/PageTotal.vue'

type page = {
  id : number,
  icon : string,
  title : string,
  href : string,
  view : object
}

export const pages : Array<page> = [
  {
    id: 1,
    icon: 'mdi-home-outline',
    title: 'Главная',
    href: '/',
    view: home
  },
  {
    id: 2,
    icon: 'mdi-vote-outline',
    title: 'Результаты',
    href: '/total',
    view: total
  },
  {
    id: 3,
    icon: 'mdi-send',
    title: 'Отправить протокол',
    href: '/protocol',
    view: protocol
  },
  {
    id: 4,
    icon: 'mdi-list-box-outline',
    title: 'Данные протоколов',
    href: '/protocols',
    view: protocols
  }
]
