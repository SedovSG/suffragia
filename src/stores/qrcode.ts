import { QrCodeDataModel } from '@/types/qrcode/DataModel'
import { defineStore } from 'pinia'

export const useQrcodeStore = defineStore({
  id: 'qrcode',
  state: () => {
    return {
      qrdata: <QrCodeDataModel> {}
    }
  },
  getters: {
    getData: (state) : QrCodeDataModel => {
      return state.qrdata
    }
  },
  actions: {
    setData (value : QrCodeDataModel) : void {
      this.qrdata = value
    }
  }
})
